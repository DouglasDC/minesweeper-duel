import { Component, OnInit } from '@angular/core';
import { Block } from './models/block';
import { ArrayHelper } from './helpers/array.helper';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  private minedField: Array< Array< Block > > = [];
  private gameIsActive: boolean;
  private victory: boolean = undefined;
  private rowsQuantity: number = undefined;
  private columnsQuantity: number = undefined;

  ngOnInit(){
    alert(`Sobre o jogo:\n
    - Campo minado é um misto de estratégia, probabilidade e sorte.
    - O campo possui tamanho definido pelo jogador, podendo ir de 8x8 a 64x64.
    - Inicialmente todos os blocos estarão ocultos.
    - Ao clicar com o botão esquerdo em um bloco ele é revelado e um dos casos abaixo pode ocorrer:
      * Caso ele esteja minado o jogo termina.
      * Caso os vizinhos possuam minas, o total de minas próximas será mostrado no bloco.
      * Caso os vizinhos não possuam minas, eles serão revelados. O processo se repete até que vizinhos com minas sejam encontrados.
    - Marque os blocos que você acha que possuem minas com o botão direito. Blocos marcados não são revelados com o clique esquerdo. Para remover a marcação basta clicar novamente com o botão direito.
    \nVence quando:
    - Todas as minas estão marcadas e todos os blocos sem minas foram revelados.
    \nPerde quando:
    - Uma mina é ativada! Simples assim :P
    `);

    this.rowsQuantity = parseInt( prompt("Quantidade de linhas (8-64)", "8"), 10 ) || 8;
    this.rowsQuantity = Math.min(64, Math.max(8, this.rowsQuantity))
    this.columnsQuantity = parseInt( prompt("Quantidade de colunas (8-64)", "8"), 10 ) || 8;
    this.columnsQuantity = Math.min(64, Math.max(8, this.columnsQuantity))

    let minesQuantity = Math.floor((this.rowsQuantity * this.columnsQuantity) / 8);

    // Creating the field
    this.minedField = new Array(this.rowsQuantity);
    
    for (let l = 0; l < this.rowsQuantity; l++) {
      this.minedField[l] = new Array(this.columnsQuantity);

      for (let c = 0; c < this.columnsQuantity; c++) {
        this.minedField[l][c] = new Block({
          row: l,
          column: c
        });
      }
    }

    // Placing Mines
    this.placeMines(minesQuantity);

    // Calculating near mines
    this.findNearMines();
    
    this.gameIsActive = true;
  }

  placeMines(minesQuantity: number){
    let placedMines = 0;
    
    while(placedMines < minesQuantity){
      let p = ArrayHelper.getRandomPosition(this.rowsQuantity, this.columnsQuantity);
      
      let selectedBlock = this.minedField[p[0]][p[1]];

      if(!selectedBlock.hasMine){
        selectedBlock.hasMine = true;
        placedMines++;
      }
    };
  }

  findNearMines() {
    // Iterate through all rows
    for (let r = 0; r < this.rowsQuantity; r++) {
      // Iterate through all columns
      for (let c = 0; c < this.columnsQuantity; c++) {
        
        ArrayHelper.getNeighbors(this.rowsQuantity, this.columnsQuantity, r, c).forEach(np => {
          if(this.minedField[np[0]][np[1]].hasMine) {
            this.minedField[r][c].nearMines++;
          }
        });     
      }
    }
  }

  revealBlockHander(position: [number, number]) {
    let r = position[0];
    let c = position[1];

    this.minedField[r][c].wasRevealed = true;
    
    // Automatically reveals blocks without mines or neighbors
    let neighborsToReveal = []
    if(!this.minedField[r][c].hasMine && this.minedField[r][c].nearMines == 0){
      neighborsToReveal = ArrayHelper.getNeighbors(this.rowsQuantity, this.columnsQuantity, r, c);
    }
    
    while(neighborsToReveal.length != 0){
      let np = neighborsToReveal.shift();
      let neighbor = this.minedField[np[0]][np[1]];

      if(!neighbor.wasRevealed){
        neighbor.wasRevealed = true;

        if(neighbor.nearMines == 0 && !neighbor.isFlagged){
          ArrayHelper.getNeighbors(this.rowsQuantity, this.columnsQuantity, np[0], np[1]).forEach(np => {
            neighborsToReveal.push(np);
          });
        }
      }
    };

    this.updateGameStatus();
  }

  putFlagHander(position: [number, number]) {
    let r = position[0];
    let c = position[1];

    this.minedField[r][c].isFlagged = !this.minedField[r][c].isFlagged;

    this.updateGameStatus();
  }

  updateGameStatus(){
    let revealedBlocks = 0;

    // Iterate through all rows
    for (let r = 0; r < this.rowsQuantity; r++) {
      // Iterate through all columns
      for (let c = 0; c < this.columnsQuantity; c++) {
        
        // When the field is mined
        if(this.minedField[r][c].hasMine){
          
          // The mine was activated
          if(this.minedField[r][c].wasRevealed){
            this.gameIsActive = false;
            this.victory = false;

            alert("MINA DETONADA!");
            return;

          // The mine is flagged
          }else if(this.minedField[r][c].isFlagged){
            revealedBlocks++;
          }

        // When the field is not mined and was revealed
        }else if(this.minedField[r][c].wasRevealed){
          revealedBlocks++;
        }
      }
    }
    
    if(revealedBlocks == (this.rowsQuantity * this.columnsQuantity )){
      this.gameIsActive = false;
      this.victory = true;
    }
  }
}