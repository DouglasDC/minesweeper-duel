import { Component, OnInit, Input, HostListener, Output, EventEmitter } from '@angular/core';
import { Block } from '../models/block';

@Component({
  selector: 'app-field-block',
  templateUrl: './field-block.component.html',
  styleUrls: ['./field-block.component.scss']
})
export class FieldBlockComponent implements OnInit {
  @Input()
  private block: Block;

  @Input()
  private isAvailable: boolean;

  @Input()
  private forceRevelation: boolean;

  @Output()
  private revealBlock = new EventEmitter<[number, number]>();
  
  @Output()
  private putFlag = new EventEmitter<[number, number]>();

  constructor() { }

  ngOnInit() { }

  @HostListener('click')
  onLeftClick() {
    if(this.isAvailable && !this.block.wasRevealed && !this.block.isFlagged){
      
      this.revealBlock.emit([this.block.row, this.block.column]);
    }
  }

  @HostListener('contextmenu', ['$event'])
  onRightClick(event: any) {
    event.preventDefault();

    if(this.isAvailable && !this.block.wasRevealed){
      this.putFlag.emit([this.block.row, this.block.column]);
    }
  }
}
