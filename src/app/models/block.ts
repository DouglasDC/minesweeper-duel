export class Block {
    public row: number = undefined;
    public column: number = undefined;
    public hasMine: boolean = undefined;
    public nearMines: number = 0;
    public wasRevealed: boolean = false;
    public isFlagged: boolean = false;

    constructor(data: object = {}) {
        for (let property in data) {
            if (data.hasOwnProperty(property)) {
                this[property] = data[property];
            }
        }
    }
}