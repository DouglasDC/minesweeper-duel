export class ArrayHelper {
    static getNeighbors(rowsQuantity, columnsQuantity, r, c, radius = 1): Array<[number, number]> {
        let neighbors: Array<[number, number]> = [];

        for(let x = Math.max(0, r-radius); x <= Math.min(r+radius, (rowsQuantity-1)); x++) {
          for(let y = Math.max(0, c-radius); y <= Math.min(c+radius, (columnsQuantity-1)); y++) {
            if(x !== r || y !== c) {
                neighbors.push([x,y]);
            }
          }
        }

        return neighbors;
    }

    static getRandomPosition(rowsQuantity, columnsQuantity): [number, number] {
        return [
            Math.floor(Math.random() * rowsQuantity),
            Math.floor(Math.random() * columnsQuantity)
        ];
      }
}
